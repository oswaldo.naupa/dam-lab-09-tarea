import firebase from 'firebase';
require("firebase/firestore");

const firebaseConfig = {
    apiKey: "AIzaSyCqKWhreRDW61Me_wdCRoAJlxgcWz_VnnA",
    authDomain: "proyect09.firebaseapp.com",
    databaseURL: "https://proyect09.firebaseio.com",
    projectId: "proyect09",
    storageBucket: "proyect09.appspot.com",
    messagingSenderId: "33579231037",
    appId: "1:33579231037:web:7ae213c76cd65a80d10754"
  };

  //Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export default firebase;