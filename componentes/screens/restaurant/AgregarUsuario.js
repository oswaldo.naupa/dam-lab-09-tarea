import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity,TextInput,Dimensions, Text, View, Image} from 'react-native';
import firebase from '../firebase';
var { height } = Dimensions.get('window');
export default class AgregarUsuario extends Component {
  constructor (props) {
    super(props);
    this.state ={
      textValue: '',
      count: 0,
      nombre:'',
      descripcion:'',
      imagen:'',
      fecha:'',
      isLoading:false,
      data: [],
      mensajerrortitulo:'',
      mensajerrorimagen:'',
      mensajerrordescripcion:'',
      mensajeerrorfecha:'',
      colortitulo: '#dcdcdc',
      colorimagen: '#dcdcdc',
      colordescripcion: '#dcdcdc',
      colorfecha:'#dcdcdc'
    };
  }

  changeTextInputNombre = text => {
    this.setState ({nombre: text});
  };
  changeTextInputDescripcion = text => {
    this.setState ({descripcion: text});
  };
  changeTextInputImagen = text => {
    this.setState ({imagen: text});
  };
  changeTextInputFecha = text => {
    this.setState ({fecha: text});
  };


  async storePelicula() {
    console.log('Click');

    if (this.state.name === ''){
      alert('Fill at least your name!');
    }else {
      this.setState({
        isLoading: true,
      });

      const db = firebase.firestore();
      db.settings({experimentalForceLongPolling: true});

      await db.collection('peliculas').add({
        nombre: this.state.nombre,
        descripcion: this.state.descripcion,
        imagen: this.state.imagen,
        fecha: this.state.fecha,
      }).then((res)=> {
        this.setState({
          nombre:'',
          descripcion:'',
          imagen:'',
          fecha:'',
          isLoading: false,
        });
      })
    }
    this.props. navigation.reset({
          routes: [{ name: 'ListaUsuarios' }],
     });
  }

  render() {
    return (
      <View style={styles.container}>
      <View>
        <Text style={styles.text}> Nombre:</Text>
      </View>
      <TextInput 
        style={styles.textInput}
        onChangeText= {text=> this.changeTextInputNombre(text)}
        placeholder="Nombre"
        placeholderTextColor="#fff"
        value= {this.state.nombre}
      />
       <View >
        <Text style={styles.text}> Descripcion:</Text>
      </View>
      <TextInput 
        style={styles.textInputDescripcion}
        placeholder="Descripcion"
        placeholderTextColor="#fff"
        multiline={true}
        numberOfLines={8}
        onChangeText= {text=> this.changeTextInputDescripcion(text)}
        value= {this.state.descripcion}
      />
      <View >
        <Text style={styles.text}> URL Imagen:</Text>
      </View>
      <TextInput 
        style={styles.textInputDescripcion}
        placeholder="URL Imagen"
        placeholderTextColor="#fff"
        multiline={true}
        numberOfLines={3}
        onChangeText= {text=> this.changeTextInputImagen(text)}
        value= {this.state.imagen}
      />
      <View >
        <Text style={styles.text}> Fecha:</Text>
      </View>
      <TextInput 
        style={styles.textInput}
        placeholder="Fecha"
        placeholderTextColor="#fff"
        onChangeText= {text=> this.changeTextInputFecha(text)}
        value= {this.state.fecha}
      />
       <View style={styles.buttoncentrado}>
        <TouchableOpacity style={styles.button}  onPress={() => {this.storePelicula()}}>
          <Text style={styles.textButon}>Agregar Pelicula</Text>
        </TouchableOpacity>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor:'#90caf9',
    padding: 100,
    paddingBottom: 250,
    height: height
  },
  text: {
    alignItems: 'center',
    padding: 10,
    fontWeight: 'bold',
    color: '#003c8f',
    fontSize: 20,
  },
  image: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
 
  textInput: {
    backgroundColor:'#003c8f',
    height: 40, 
    borderColor: '#DDDDDD',
    borderWidth: 1,
    fontSize: 18,
    color: '#DDDDDD',
    borderRadius : 10,
  },
  textInputDescripcion: {
    textAlignVertical :'top',
    borderColor: '#DDDDDD', 
    backgroundColor:'#003c8f',
    borderWidth: 1,
    fontSize: 18,
    color: '#DDDDDD',
    borderRadius : 10,
  },
  button: {
    backgroundColor: '#191970',
    alignItems: "center",
    alignContent: 'center',
    justifyContent: "space-around",
    padding: 5,
    width:190,
    borderRadius: 10,
    margin:10,
  },
  textButon: {
      alignItems: 'center',
      padding: 5,
      fontWeight: 'bold',
      fontSize: 20,
      color: '#fff',
    },
    buttoncentrado: {
      alignItems: 'center',
      justifyContent: 'center',
    },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
})
