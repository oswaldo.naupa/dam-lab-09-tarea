import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity,TextInput,Dimensions, Text, View, Image} from 'react-native';
import firebase from '../firebase';
import MaterialCommunityIconsFilm from 'react-native-vector-icons/Ionicons';

var { height } = Dimensions.get('window');
export default class EditUsuario extends Component {
  constructor (props) {
    super(props);
    this.state ={
      textValue: '',
      count: 0,
      nombre:'',
      descripcion:'',
      imagen:'',
      fecha:'',
      id:'',
      isLoading:false,
    };
  }

  changeTextInputNombre = text => {
    this.setState ({nombre: text});
  };
  changeTextInputDescripcion = text => {
    this.setState ({descripcion: text});
  };
  changeTextInputImagen = text => {
    this.setState ({imagen: text});
  };
  changeTextInputFecha = text => {
    this.setState ({fecha: text});
  };
  

  async editUser(id){
    const db = firebase.firestore();
    const ref = db.collection('peliculas').doc(id);
    db.settings({experimentalForceLongPolling: true});
    await ref.update({
      nombre: this.state.nombre,
      descripcion: this.state.descripcion,
      imagen:this.state.imagen,
      fecha: this.state.fecha
    })
    .then(function(){
      console.log("Pelicula Editada")
    })
    .catch(function(error){
      console.log("Error")
    })
    this.props. navigation.reset({
      routes: [{ name: 'ListaUsuarios' }],
    });
  }

 
  componentDidMount(){
    const { nombre } = this.props.route.params;
    const { descripcion } = this.props.route.params;
    const { imagen } = this.props.route.params;
    const { fecha } = this.props.route.params;
    const { id } = this.props.route.params;
    this.setState({
      nombre: nombre,
      descripcion:descripcion,
      imagen:imagen,
      fecha:fecha,
      id:id
    });
  }


  render() {
       
    return (
      <View style={styles.container}>
      <View>
        <Text style={styles.text}> Nombre:</Text>
      </View>
      <TextInput 
        style={styles.textInput}
        onChangeText= {text=> this.changeTextInputNombre(text)}
        placeholder="Nombre"
        placeholderTextColor="#20b2aa"
        value= {this.state.nombre}
      />
       <View >
        <Text style={styles.text}> Descripcion:</Text>
      </View>
      <TextInput 
        style={styles.textInputDescripcion}
        placeholder="Descripcion"
        placeholderTextColor="#20b2aa"
        multiline={true}
        numberOfLines={8}
        onChangeText= {text=> this.changeTextInputDescripcion(text)}
        value= {this.state.descripcion}
      />
      <View >
        <Text style={styles.text}> URL imagen:</Text>
      </View>
      <TextInput 
        style={styles.textInputDescripcion}
        placeholder="URL imagen"
        multiline={true}
        numberOfLines={3}
        placeholderTextColor="#20b2aa"
        onChangeText= {text=> this.changeTextInputImagen(text)}
        value= {this.state.imagen}
      />
      <View >
        <Text style={styles.text}> Fecha:</Text>
      </View>
      <TextInput 
        style={styles.textInput}
        placeholder="Fecha"
        placeholderTextColor="#20b2aa"
        onChangeText= {text=> this.changeTextInputFecha(text)}
        value= {this.state.fecha}
      />
      <View style={styles.buttoncentrado}>
      <TouchableOpacity style={styles.button}  onPress={() => {this.editUser(this.state.id)}}>
        <Text style={styles.textButon}>Editar Pelicula</Text>
      </TouchableOpacity>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor:'#90caf9',
    padding: 100,
    paddingBottom: 250,
    height: height
  },
  text: {
    alignItems: 'center',
    padding: 10,
    fontWeight: 'bold',
    color: '#191970',
    fontSize: 20,
  },
  image: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },

  textInput: {
    backgroundColor:'#003c8f',
    height: 40, 
    borderColor: '#DDDDDD', 
    borderWidth: 1,
    fontSize: 18,
    color: '#DDDDDD',
    borderRadius : 10,
  },
  textInputDescripcion: {
    backgroundColor:'#003c8f',
    textAlignVertical :'top',
    borderColor: '#DDDDDD', 
    borderWidth: 1,
    fontSize: 18,
    color: '#DDDDDD',
    borderRadius : 10,
  },

  button: {
    backgroundColor: '#191970',
    alignItems: "center",
    alignContent: 'center',
    justifyContent: "space-around",
    padding: 5,
    width:190,
    borderRadius: 10,
    margin:10,
  },
  textButon: {
      alignItems: 'center',
      padding: 5,
      fontWeight: 'bold',
      fontSize: 20,
      color: '#fff',
    },
    buttoncentrado: {
      alignItems: 'center',
      justifyContent: 'center',
    },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
})
