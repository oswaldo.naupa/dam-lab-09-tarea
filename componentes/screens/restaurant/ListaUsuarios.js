import React, {Component} from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/AntDesign';
import {View, FlatList, StyleSheet, Text,RefreshControl, Image, TouchableOpacity,Alert } from 'react-native';
import { Searchbar, ActivityIndicator } from 'react-native-paper';
import firebase from '../firebase';
import { useIsFocused } from '@react-navigation/native'
import MaterialCommunityIconsFilm from 'react-native-vector-icons/Ionicons';

export default class ListaUsuarios extends Component{
    
    constructor(props){
        super(props);
        this.state = {
          text: "",
          items: [],
          data: [],
          refreshing: true,
      };        
    }

    
    showAlert = (id) =>{
      Alert.alert(
        'Confirmacion',
        '¿Esta seguro que que quiere eliminar el registro?',
        [
          {
            text:'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style:'cancel',
          },
          {text:'OK', onPress:() => this.eliminar(id)}
        ],
        {cancelable: false},
      );
    };

    Item(navigation,nombre,descripcion,imagen,fecha,id) {
      return (
          <View style= {styles.container}>
          <View style={{flex:1, flexDirection: 'row'}}>
              <View style={styles.imageView}>
                  <TouchableOpacity
                  onPress={() => {
                    /* 1. Navigate to the Details route with params */
                    navigation.navigate('DetallesPelicula', {
                      nombre: nombre,
                      descripcion: descripcion, imagen:imagen,fecha:fecha
                    });
                    }}>
                  <Image style={styles.imageViewcont} source = {{uri: imagen}}/>
              </TouchableOpacity>
              </View>
              <View style= {styles.textView}>
                  <Text style= {styles.title}>{nombre}</Text>
              </View>
                <View style={styles.buttoneditar} >
                <TouchableOpacity 
                  onPress={() => {
                        /* 1. Navigate to the Details route with params */
                        navigation.navigate('EditUsuario', {
                          id: id, nombre: nombre,
                          descripcion: descripcion, imagen:imagen,fecha:fecha
                        });
                        }}>
                  <MaterialCommunityIcons name="edit"  size={30} color='#fff' />
                </TouchableOpacity >
                </View>
                <View style={styles.buttoneliminar} >
                  <TouchableOpacity 
                      onPress={() => {
                            /* 1. Navigate to the Details route with params */
                            this.showAlert(id)
                            }}>
                      <MaterialCommunityIcons name="delete"  size={30} color='#fff'/>
                    </TouchableOpacity >
                </View>
          </View>
          </View>
      );
    }

    async cargarDatos(){
      let data = []
      const db = firebase.firestore();
      db.settings({experimentalForceLongPolling: true});
      var snapshot = await db.collection('peliculas').get()
      snapshot.forEach((doc)=>{
        const dat = doc.data();
        const id = doc.id;
        data.push({id,...dat})

        console.log(doc.id)
      });
      
      this.setState({ data: data });
      this.setState({ items: data });
      this.setState({ refreshing: false });
      console.log(this.state.data)
    }
    async eliminar(name) {
      const db = firebase.firestore();
      const dbRef = db.collection('peliculas').doc(name)
      await  dbRef.delete().then((res) => {
            console.log('Item removed from database')
        })
      this.setState({ data:[]});
      this.cargarDatos()

    }
    async eliminar(name) {
      const db = firebase.firestore();
      const dbRef = db.collection('peliculas').doc(name)
      await  dbRef.delete().then((res) => {
            console.log('Item removed from database')
        })
      this.setState({ data:[]});
      this.cargarDatos()

    }

    async componentDidMount() {
      await this.cargarDatos()
      
  }
  
  
  

    renderSeparatorView = () => {
      return (
        <View style={{
            height: 1, 
            width: "100%",
            backgroundColor: "#a9a9a9",
          }}
        />
      );
    };

    onRefresh() {
      //Clear old data of the list
      this.setState({ data: [] });
      this.cargarDatos()
      //Call the Service to get the latest data
      
    }

    searchFilterFunction = query  => {
      const textData = query.toUpperCase();
      const newData = this.state.items.filter(item => item.nombre.toUpperCase().indexOf(textData) > -1 );
      this.setState({ data: newData });
      this.setState({text: query})  
    };

        
    renderHeader = () => {    
      return (      
        <Searchbar        
          placeholder="Buscar Peliculas"
          onChangeText={this.searchFilterFunction}
          value= {this.state.text}         
        />    
      );  
    };

    render(){
        
        return(
            <View style={styles.list}>
              <View style={styles.derecha}>
              <View style={styles.button} >
                  <TouchableOpacity onPress={() => {
                          /* 1. Navigate to the Details route with params */
                          this.props.navigation.navigate('AgregarUsuario', {
                          });
                  }}>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={styles.textButon}>Agregar</Text>
                        <MaterialCommunityIconsFilm name="ios-film"  size={35} color='#fff'/>
                      </View>
                  </TouchableOpacity >
                  </View>
                </View>
                  <FlatList style={styles.container}
                      
                      data={this.state.data}
                      extraData={this.props}
                      renderItem={({ item }) => this.Item( 
                      this.props.navigation,item.nombre,item.descripcion,item.imagen,item.fecha,item.id)}
                      keyExtractor={item => item.id}
                      ListHeaderComponent={this.renderHeader}
                      stickyHeaderIndices={[0]}
                      ItemSeparatorComponent={this.renderSeparatorView}
                      refreshControl={
                        <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this.onRefresh.bind(this)}
                        />}
                  />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        padding:50,
        justifyContent: 'center',
        alignContent: 'center',
      },
    list: {
      flex: 1,
      backgroundColor: '#003c8f',
      
    },
    derecha: {
      alignItems: 'flex-end',
      marginRight: 20,
      
    },
    container: {
      marginTop: 15,
      margin: 18,
      backgroundColor: '#90caf9'
    },
    imageView: {
        width: '40%',
        height: 80 ,
        margin: 5,
        borderRadius: 30, 
    },
    imageViewcont: {
      width: '100%',
      height: 80 ,
      margin: 5,
      borderRadius: 30, 
  },
    textView: {
        width:'22%', 
        textAlignVertical:'center',
        textAlign:'justify',
        justifyContent: 'center',
        fontFamily: 'Pacifico-Regular',
        margin: 10,
    },
    textvolver:{
      marginRight: 20,
      justifyContent: 'center'
    },
    item: {
      backgroundColor: 'gray',
      padding: 10
    },
    title: {
      fontSize: 20,
      color: '#000000',
      fontWeight: 'bold',
      fontFamily: 'Bangers',
    },
    titleloading: {
        fontSize: 40,
        color: '#c43c00',
        fontWeight: 'bold',
        fontFamily: 'Bangers',
      },

    
      buttoneditar: {
        backgroundColor: '#191970',
        alignItems: "center",
        alignContent: 'center',
        justifyContent: "space-around",
        padding: 5,
        width:60,
        height:60,
        borderRadius: 10,
        margin:5,
      },
      buttoneliminar: {
        backgroundColor: '#dc143c',
        alignItems: "center",
        alignContent: 'center',
        justifyContent: "space-around",
        padding: 5,
        width:60,
        height:60,
        borderRadius: 10,
        margin:5,
      },
      
      button: {
        backgroundColor: '#191970',
        alignItems: "center",
        alignContent: 'center',
        justifyContent: "space-around",
        padding: 5,
        width:190,
        borderRadius: 10,
        margin:10,
      },
      textButon: {
          alignItems: 'center',
          padding: 5,
          fontWeight: 'bold',
          fontSize: 20,
          color: '#fff',
        },
  });