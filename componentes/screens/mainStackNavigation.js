import React, {Component} from 'react';
import { NavigationContainer} from '@react-navigation/native';
import { createStackNavigator,NavigationEvents } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';


import ListaUsuarios from './restaurant/ListaUsuarios';
import EditUsuario from './restaurant/EditUsuario';
import AgregarUsuario from './restaurant/AgregarUsuario';
import DetallesPelicula from './restaurant/DetallesPelicula';
import TopNavigation from './TopNavigation'



const stackrestaurant = createStackNavigator();
function RestaurantStackScreen(){
  return(
    <stackrestaurant.Navigator>
    <stackrestaurant.Screen name="ListaUsuarios" component={ListaUsuarios} 
      options={({navigation}) => (
        { 
        title: 'Peliculas',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="AgregarUsuario" component={AgregarUsuario} 
      options={({navigation}) => (
        { 
        title: 'Agregar',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="EditUsuario" component={EditUsuario} 
      options={({navigation}) => (
        { 
        title: 'Editar',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="DetallesPelicula" component={DetallesPelicula} 
      options={({navigation}) => (
        { 
        title: 'Detalles',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
  </stackrestaurant.Navigator>
  );
}

const Tab = createMaterialBottomTabNavigator(); 
export default class MainStackNavigator extends Component {
  constructor (props) {
    super(props);
    
  }
  render() {
  return (
    <NavigationContainer>
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="white"
      barStyle={{ backgroundColor: '#1565c0' }}  >
     
      <Tab.Screen name="Restauntes" component={RestaurantStackScreen}
                     options={{
                    tabBarLabel: 'Peliculas',
                    tabBarIcon: ({ color }) => (
                      <MaterialCommunityIcons name="film" color={color} size={26} />
                    ),
                  }} />
     
    </Tab.Navigator>
    </NavigationContainer>
  )}
}

