
import React, {Component} from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import Icon from 'react-native-vector-icons/MaterialIcons';
import firebase from './firebase'

class TopNavigation  extends Component{
    constructor(props){
        super(props);        
    }
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this.props.navigation.navigate('Home')
  };

  showMenu = () => {
    this._menu.show();
  };
/*
 Freepik
 Smashicons
*/
  render() {
    return (
      <View style={{lignItems: 'center', justifyContent: 'center' }}>
          <Menu 
           style={{backgroundColor: '#003c8f' }}
          ref={this.setMenuRef}
          button={<Icon
            Icon name="menu" size={40} color="#fff" onPress={this.showMenu}></Icon>}
          >
            <MenuItem><Text style={styles.textTitle}>Ajustes</Text></MenuItem>
            <MenuItem onPress={this.hideMenu}><Text style={styles.textTitle}>Opciones</Text></MenuItem>
            <MenuItem onPress={this.hideMenu} disabled><Text style={styles.textTitle}>Ayuda</Text>
            </MenuItem>
              <MenuDivider />
            <MenuItem onPress={() => {
                    /* 1. Navigate to the Details route with params */
                    firebase.auth().signOut()
                    this.props. navigation.reset({
                      index: 0,
                      routes: [{ name: 'Restauntes' }],
                    });
                    
                  }}><Text style={styles.textTitle}>Cerrar Sesion</Text></MenuItem>
          </Menu>
      </View>
    );
  }
}

export default TopNavigation ;

const styles = StyleSheet.create({
  textTitle:{
      fontSize: 20,
      color:"#fff"
  },
})
